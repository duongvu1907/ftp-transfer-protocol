/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.net.InetAddress;

/**
 *
 * @author duong
 */
public class ServerFtp {
    private InetAddress inet;
    private int port;
    private String userName;
    private String password;

    public InetAddress getInet() {
        return inet;
    }

    public void setInet(InetAddress inet) {
        this.inet = inet;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ServerFtp(InetAddress inet, int port, String userName, String password) {
        this.inet = inet;
        this.port = port;
        this.userName = userName;
        this.password = password;
    }
}
