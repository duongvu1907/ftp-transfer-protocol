/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repo;

import Entity.ServerFtp;
import java.io.IOException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTP;
import Ultis.*;
/**
 *
 * @author duong
 */
public class ConnectFtpServer {
    private ServerFtp server;

    public ConnectFtpServer(ServerFtp server) {
        this.server = server;
    }

    public ServerFtp getServer() {
        return server;
    }

    public void setServer(ServerFtp server) {
        this.server = server;
    }
    public FTPClient make(){
        FTPClient client = new FTPClient();
        try {
            client.connect(this.server.getInet(),this.server.getPort());
            showServerReply(client);
            int replyCode = client.getReplyCode();
            if(!FTPReply.isPositiveCompletion(replyCode)){
                System.out.println("Operation failed. Server reply code: " + replyCode);
                $.writeLog("Operation failed. Server reply code: " + replyCode);
            }
            if(!client.login(this.server.getUserName(), this.server.getPassword())){
                System.err.println("Login failed ");  
                $.writeLog("Login failed ! "+showServerReply(client));
            }else{
                System.out.println("Login success !");
                $.writeLog("Login success ! "+this.server.getInet().getHostName()+" - "+showServerReply(client));
                $.startTime = System.currentTimeMillis();
                client.enterLocalPassiveMode();
                client.setFileType(FTP.BINARY_FILE_TYPE);
               
            }
            System.out.println(showServerReply(client));
        
        } catch (IOException ex) {
            System.err.println("Oops! Something went wrong "+ex.getMessage());
        }
        return client;
    }
    private static String showServerReply(FTPClient ftp) {
            String[] replies = ftp.getReplyStrings();
            String s ="";
            if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                s = "SERVER: " + aReply;
            }
        }
            return s;
    }
}
