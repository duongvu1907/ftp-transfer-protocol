/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author duong
 */
public class $ {
    public static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    public static final int frWidth = (int)(screen.width*0.8);
    public static final int frheight = (int)(screen.height*0.8);
    public static ArrayList<String> list_type = new ArrayList<>();
    public  DefaultMutableTreeNode computer = new DefaultMutableTreeNode("Computer");
    
    public static  Icon ICON_COMPUTER = new ImageIcon("data/icon/cloud.png");
    public static Icon ICON_DISK = new ImageIcon("data/icon/hard-disk.png");
    public static Icon ICON_FOLDER = new ImageIcon("data/icon/folder.png");
    public static Icon ICON_FILE = new ImageIcon("data/icon/file.png");
    public static Icon ICON_SERVER = new ImageIcon("data/icon/server.png");
    public static final File log = new File("data/log.txt");
    
    public static long startTime;
    public static long endTime;
    public static void writeLog(String s) throws FileNotFoundException, IOException{
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(log,true))) {
            PrintWriter console = new PrintWriter(bw);
            String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
            console.println(timeStamp+" : "+s+"\n");
            console.flush();
        }
    }
}
