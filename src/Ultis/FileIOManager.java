/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import Entity.FileLocal;
import Entity.FileServer;
import java.io.File;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author duong
 */
public interface FileIOManager {
    public void SendFileSingleThread(String path,File file);
    public void SendFileMultiThread(FileLocal file,FTPClient client);
    public void DownloadFile(String pathServer,String pathLocal);
}
