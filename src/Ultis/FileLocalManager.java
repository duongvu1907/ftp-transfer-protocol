/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import java.io.File;

/**
 *
 * @author duong
 */
public class FileLocalManager extends FileManagerUltis {

    @Override
    public boolean SameFile(String path, String name) {
        boolean check = false;
        File[] listFile;
        listFile = new File(path).listFiles();
        for (File file : listFile) {
            if(file.isFile()&&file.exists()&&file.getName().compareToIgnoreCase(name)==0){
                check = true;
            }
        }
       return check;
    }

    @Override
    public boolean SameFolder(String path, String name) {
        boolean check = false;
        File[] listFile;
        listFile = new File(path).listFiles();
        for (File file : listFile) {
            if(file.isDirectory()&&file.exists()&&file.getName().compareToIgnoreCase(name)==0){
                check = true;
            }
        }
       return check;
    }

    @Override
    public boolean RenameFile(String path, String oldName, String newName) {
        boolean check = false;
        File file = new File(path+"/"+oldName);
        if (file.exists() && file.isFile() && !file.getName().equals(newName)) {
            file.renameTo(new File(path+"/"+newName));
            check  = true;
            System.out.println("Rename file "+path+"/"+oldName+"=>"+newName+" : successful !");
        }else{
            System.err.println("Rename file "+path+"/"+oldName+"=>"+newName+" : failed !");
        }
        return check;
    }

    @Override
    public boolean DeleteFile(String path, String name) {
        boolean check = false;
        File file = new File(path+"/"+name);
        if (file.exists() && file.isFile()&&file.delete()) {
            check  = true;
            System.out.println("Delete file "+path+"/"+name+" : successful !");
        }else{
            System.err.println("Delete file "+path+"/"+name+" : failed !");
        }
        return check;
    }

    @Override
    public boolean RenameFolder(String path, String oldName, String newName) {
        boolean check=false;
        File dir = new File(path+"/"+oldName);
        File newDir = new File(path+"/"+newName);
        if(dir.renameTo(newDir)){
            check = true;
            System.out.println("RenameFolder "+path+"/"+oldName+" => :sucessful !");
        }else{
            System.err.println("RenameFolder "+path+"/"+oldName+" => :failed !");   
        }
        return check;
    }
    private static String GetFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
     public static  boolean CheckTypeFile(String path,String name){
         boolean check = false;
         File file = new File(path+"/"+name);
         System.out.println(GetFileExtension(file));
         if (file.exists() && file.isFile()) {
             for (String type : $.list_type) {
                 if(GetFileExtension(file).compareToIgnoreCase(type)==0){
                     check=true;
                 }
             }
         }
         return check;
     }
}
