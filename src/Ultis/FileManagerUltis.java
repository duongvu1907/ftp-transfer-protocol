/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

/**
 *
 * @author duong
 */
abstract class FileManagerUltis {
    
    public abstract boolean SameFile(String path,String name);
    public abstract boolean SameFolder(String path,String name);
    public abstract boolean RenameFile(String path,String oldName,String newName);
    public abstract boolean DeleteFile(String path,String name);
    public abstract  boolean RenameFolder(String path,String oldName,String newName);
    
}
