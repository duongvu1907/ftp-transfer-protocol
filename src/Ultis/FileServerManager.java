/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import Entity.FileServer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

/**
 *
 * @author duong
 */
public class FileServerManager extends FileManagerUltis{
    private FTPClient client;

    public FileServerManager(FTPClient client) {
        this.client = client;
    }

    public FTPClient getClient() {
        return client;
    }

    public void setClient(FTPClient client) {
        this.client = client;
    }

    @Override
    public  boolean SameFile(String path, String name) {
       boolean check = false;
        try {
            FTPFile[] dir = client.listFiles(path);
            for (FTPFile f : dir) {
                if(f.getType()==FTPFile.FILE_TYPE && f.getName().compareToIgnoreCase(name)==0){
                    check =true;     
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileServerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }

    @Override
    public boolean SameFolder(String path, String name) {
         boolean check = false;
        try {
            FTPFile[] dir = client.listFiles(path);
            for (FTPFile f : dir) {
                if(f.getType()==FTPFile.DIRECTORY_TYPE && f.getName().compareToIgnoreCase(name)==0){
                    check =true;     
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileServerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }

    @Override
    public boolean RenameFile(String path,String oldName, String newName) {
        boolean check = false;
        try {
            if (client.rename(path+"/"+oldName, path+"/"+newName)) {
                System.out.println("Rename file success ! "+oldName+" to "+newName);
               check = true; 
            }else{
                System.err.println("Rename failed !");
            }
            return check;
        } catch (IOException ex) {
            Logger.getLogger(FileServerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }

    @Override
    public boolean DeleteFile(String path,String name) {
        boolean check = false;
        try {
            if (client.deleteFile(path+"/"+name)) {
                System.out.println("DeleteFile "+path+"/"+name+" : successful !");
                check=true;
            }else{
                System.err.println("DeleteFile "+path+"/"+name+" : failed !");
            }
           
        } catch (IOException ex) {
            Logger.getLogger(FileServerManager.class.getName()).log(Level.SEVERE, null, ex);
        }
         return check;
    }

    @Override
    public boolean RenameFolder(String path, String oldName, String newName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
