/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author duong
 */
public class ThreadEchoTransfer implements Runnable{
    private File file;
    private String key;
    private String path;
    private FTPClient client;
    public ThreadEchoTransfer(File file,FTPClient client,String path) {
        this.file = file;
        this.client = client;
        this.path = path;
    }
    public ThreadEchoTransfer(File file,FTPClient client,String key,String path) {
        this.file = file;
        this.client = client;
        this.key=key;
        this.path = path;
    }
    @Override
    public void run() {
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(file));
            client.storeFile(path, is);
        } catch (FileNotFoundException ex) {
            System.err.println("Thread Send File Failed "+ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Thread Send File Failed "+ex.getMessage());

        }
    }
    
}
