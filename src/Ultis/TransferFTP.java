/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ultis;

import Entity.FileLocal;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author duong
 */
public class TransferFTP implements FileIOManager{
    private final FTPClient client;
    public TransferFTP(FTPClient client) {
        this.client = client;
    }
    @Override
    public void SendFileSingleThread(String path,File file) {
        Thread t = new Thread(new ThreadEchoTransfer(file, client, path));
        t.start();
        showThreadStatus(t);
    }
    static void showThreadStatus(Thread thrd) {
      System.out.println(thrd.getName()+" Alive:"+thrd.isAlive()+" State:" + thrd.getState() );
   }
    @Override
    public void SendFileMultiThread(FileLocal file, FTPClient client) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void DownloadFile(String pathServer, String pathLocal) {
        OutputStream os = null;
        try {
            File file = new File(pathLocal);
            os = new BufferedOutputStream(new FileOutputStream(file));
            if (client.retrieveFile(pathServer,os)) {
                System.out.println("Download file "+pathServer);
            }else{
                System.err.println("Download file failed !");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TransferFTP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TransferFTP.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                os.close();
            } catch (IOException ex) {
                Logger.getLogger(TransferFTP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
